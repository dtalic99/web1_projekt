//Funkcije
function oServisu(tekst) {
  $(document).ready(function () {
    $("#o_servisu").click(function () {
      $("#sadrzaj").html(tekst);
    });
  });
}

const tekst =
  "<div class='flex-grow-1'><h3 class='mb-0'>O servisu</h3><p>Vukovarska 17a</p></div>";

oServisu(tekst);

var modal = new tingle.modal({
  footer: true,
  stickyFooter: false,
  closeMethods: ["overlay", "button", "escape"],
  closeLabel: "Close",
  cssClass: ["custom-class-1", "custom-class-2"],
  onOpen: function () {
    console.log("modal open");
  },
  onClose: function () {
    console.log("modal closed");
  },
  beforeClose: function () {
    // here's goes some logic
    // e.g. save content before closing the modal
    return true; // close the modal
    return false; // nothing happens
  },
});

// set content
var forma = modal.setContent(
  '<form><div class="form-group"><label>Email address</label><input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"><small id="emailHelp" class="form-text text-muted">We\'ll never share your email with anyone else.<small></div><div class="form-group"><label for="exampleInputPassword1">Password</label><input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"></div><div class="form-check"><input type="checkbox" class="form-check-input" id="exampleCheck1"><label class="form-check-label" for="exampleCheck1">Check me out</label></div></form>'
);

modal.addFooterBtn("Prijava", "tingle-btn tingle-btn--primary", function () {
  // logika za prijavu
  modal.close();
});

// add another button
modal.addFooterBtn("Odustani", "tingle-btn tingle-btn--danger", function () {
  // ostavi ovo
  modal.close();
});

// open modal
$(document).ready(function () {
  $("#login").click(function () {
    modal.open();
  });
});

// close modal
modal.close();
