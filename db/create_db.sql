CREATE TABLE korisnici (
	id INT NOT NULL,
	ime VARCHAR2(40) NOT NULL,
	prezime VARCHAR2(40) NOT NULL,
	dat_rod DATE,
	pozivni_id INT,
	mobilni_broj NUMBER(7, 0) NOT NULL,
	drzava_id INT NOT NULL,
    	email VARCHAR2(40) NOT NULL,
	password VARCHAR2(40) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	constraint KORISNICI_PK PRIMARY KEY (id));

CREATE sequence KORISNICI_ID_SEQ;

CREATE trigger BI_KORISNICI_ID
  before insert on korisnici
  for each row
begin
  select KORISNICI_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE pozivni_brojevi (
	id INT NOT NULL,
	broj VARCHAR2(3) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	constraint POZIVNI_BROJEVI_PK PRIMARY KEY (id));

CREATE sequence POZIVNI_BROJEVI_ID_SEQ;

CREATE trigger BI_POZIVNI_BROJEVI_ID
  before insert on pozivni_brojevi
  for each row
begin
  select POZIVNI_BROJEVI_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE drzave (
	id INT NOT NULL,
	naziv VARCHAR2(255) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	constraint DRZAVE_PK PRIMARY KEY (id));

CREATE sequence DRZAVE_ID_SEQ;

CREATE trigger BI_DRZAVE_ID
  before insert on drzave
  for each row
begin
  select DRZAVE_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE vozila (
	id INT NOT NULL,
	korisnik_id INT NOT NULL,
	model VARCHAR2(40) NOT NULL,
	problem VARCHAR2(500) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	updated_at TIMESTAMP NOT NULL,
	constraint VOZILA_PK PRIMARY KEY (id));

CREATE sequence VOZILA_ID_SEQ;

CREATE trigger BI_VOZILA_ID
  before insert on vozila
  for each row
begin
  select VOZILA_ID_SEQ.nextval into :NEW.id from dual;
end;
CREATE sequence VOZILA_KORISNIK_ID_SEQ;

CREATE trigger BI_VOZILA_KORISNIK_ID
  before insert on vozila
  for each row
begin
  select VOZILA_KORISNIK_ID_SEQ.nextval into :NEW.korisnik_id from dual;
end;

/
ALTER TABLE korisnici ADD CONSTRAINT korisnici_fk0 FOREIGN KEY (pozivni_id) REFERENCES pozivni_brojevi(id);
ALTER TABLE korisnici ADD CONSTRAINT korisnici_fk1 FOREIGN KEY (drzava_id) REFERENCES drzave(id);



ALTER TABLE vozila ADD CONSTRAINT vozila_fk0 FOREIGN KEY (korisnik_id) REFERENCES korisnici(id);
